﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>


char * cutleft(char *str, char *cutDel)
//Return the left part of "str" defined by "cutDel" (wich will be truncated with the right part)
{
char *res = strdup(str); //copy of str inside res (malloc)

char * firstc; //firstrc will be a pointer to the first occurence of the cutDel in str (after the line just below)
firstc = strstr(res, cutDel);
*firstc = '\0';//we cut off the right part

return res;
}


char * cutright(char *str, char *cutDel)
//Return the right part of "str" defined by "cutDel" (wich will be truncated with the left part)
{
char * firstc; //firstrc will be a pointer to the first occurence of the cutDel in str (after the line just below)
firstc = strstr(str, cutDel);
long res_double = (long)(firstc) + (long)strlen(cutDel) ;//we cut off the left part
char * res = (char *)res_double;

res = strdup(res);//strdup is provide a malloc
return res;
}



