\documentclass[12pt]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{titling}
\usepackage{multicol}
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{caption}
\usepackage{xcolor}

\usepackage[top=3cm, bottom=3cm]{geometry}
\onehalfspacing
\linespread{1.2}
\author{\LARGE Dorian \textsc{geraut}\\Gabrielle \textsc{toulet morlanne}}
\title{Rapport de projet RSA\\ Proxy \textsc{HTTP} \\  }


\begin{document}
 \makeatletter
  \begin{titlepage}
    \begin{flushleft}
      \@author
    \vspace{6cm}
    \end{flushleft}
  \centering
       {\LARGE
       \@title}  \vspace{2cm}
        \Large{Telecom Nancy - 2A TRS}
    \vfill
       \includegraphics[height=0.09\textheight]{tele1.png}
        \hfill
      \includegraphics[height=0.07\textheight]{tele2.png}
  \end{titlepage}
\makeatother

\newpage

\section{Introduction}

Le document suivant présente notre solution de proxy HTTP. Un proxy est 
un intermédiaire entre le navigateur (un client) et le reste 
du web. Il peut servir à améliorer la qualité de service en stockant des 
pages dans son cache ou des informations relatives à la connexion, il peut 
également avoir un rôle sécuritaire (en filtrant des sites, des éléments particuliers, etc...). 
Notre proxy, toutefois, est transparent. Son objectif est 
d'être présent sans limiter l'accès aux différents sites et sans limiter les possibilités de l'utilisateur, 
ce qui signifie fonctionner en supportant le protocole IPv6 et assurer 
une connexion multi-client.

\tableofcontents

\newpage

\section{Structure du code}

\subsection{Structure générale}

Le code fonctionne de façon linéaire dans un premier temps. Dans la première partie nous crééons une socket de connexion pour le client.
On lie cette socket (avec l'appel à bind()) à l'adresse locale et au port précis par lequel il devra passer. Lorsqu'un client se connecte, nous créons une socket de dialogue
avec celui ci.


Nous partons du principe que le premier envoi du client va être une requete \textsc{GET} puisqu'il s'agit d'un proxy prévu pour le protocole HTTP. Nous récupérons cette première 
requete. Après vérification qu'il s'agit bien d'un \textsc{GET} et que la requête est bien formée, les informations relatives au serveur sont récupérées (hostname)
pour créer une socket de dialogue avec celui-ci.
Le processus effectue alors un appel à fork(), dont le processus fils aura le rôle de s'occuper de la communication avec le serveur, et le père avec le client.

Ces deux parties sont très similaire, elles sont constitués d'une boucle infinie dans laquelle on lit une socket de dialogue et on
relaie le contenu dans l'autre socket.
Le fils relaie l'information du client au serveur et vis-versa pour le père. Lorsqu'une des partie termine la connexion, le père ou le fils se charge de fermer les autres
sockets de dialogue et se termine.


\subsection{Support du multi-client}

Nous gérons la possibilité de connexion pour plusieurs client grâce à un appel supplémentaire à la fonction fork(), non décrit précédemment.
Une fois la socket locale de connexion au client créée et liée à l'adresse locale et au port, le processus entre dans une boucle. Dès qu'une connexion
de la port d'un client est acceptée, le processus se fork(). Le processus père va fermer son descripteur de la socket de dialogue et attendre le prochain client.
Le processus fils, lui, va traiter le client qui vient de se connecter. 


\subsection{Support de l'IPv6}
Le proxy \textsc{HTTP} supporte le protocole réseau IPv6. Nous avons pu effectuer nos tests sur notre réseau à domicile,
dont le fournisseur d'accès propose l'utilisation d'IPv6. Nous avons procédé de la façon suivante :


Tout d'abord, le proxy HTTP créé une socket de connexion du coté du client (navigateur web), comme expliqué plus haut. Cette socket est de type
 \emph{AF\_INET6} : c'est donc une socket adaptée pour le protocole IPv6. Cependant, nous lui passons l'option \emph{IPV6\_V6ONLY} à 0, de cette
façon elle peut accepter des connections en provenance d'adresses IPv6 mais également d'adresses IPv4. Les adresses IPv4 seront en réalité ``mappées''
sur des adresses IPv6. 

Nous avons trouvé après nos recherches que cette méthode de \emph{Dual-Stack} au niveau de la socket de connexion aux clients était la plus simple et également
la plus documentée\footnote{Dual-Stack Sockets for IPv6 Winsock Applications, Microsoft MSND.}.

\begin{figure}[!h]
 \centering
 \includegraphics[scale=0.5]{ipv62.png}
 \caption{Test sur \url{ipv6.google.com} }
\end{figure}

\begin{figure}[!h]
 \centering
 \includegraphics[scale=0.35]{ipv61.png}
 \caption{Tests du bon support de l'IPv6 sur le site \url{test-ipv6.com} }
\end{figure}


D'autre part, lors de la connexion du proxy au serveur distant, nous utilisons une socket de type \emph{AF\_UNSPEC}. Cette valeur indique à la 
fonction \emph{getaddrinfo} (qui renvoie les informations de l'adresse en fonction du nom de domaine demandé) qu'elle peut renvoyer une adresse IPv4
ou IPv6. C'est donc \emph{getaddrinfo} qui se charge d'obtenir l'adresse IPv6 auprès du DNS.
Par la suite, nous avons recours à la fonction \emph{inet\_ntop} et nous affichons à l'utilisateur l'adresse IPv4 ou IPv6 (en fonction du cas) du serveur
auquel il souhaite se connecter.\\


Nous arrivons à accéder au site \url{ipv6.google.com}. Nous avons également effectué des tests sur des sites qui proposent de mesurer la compatibilité 
IPv6. Nous obtenons des résultats corrects excepté pour la taille de la MTU, qui est réduite, en raison de notre buffer interne. 

\section{Difficultés rencontrées}
Une des premières difficultés rencontrées fut de de trouver la structure correcte du code afin de permettre d'échange de données entre la socket de 
dialogue avec le serveur et celle de dialogue avec le client.
En effet, les sockets étant par défaut en mode ``non-bloquant'', lors de l'appel à recv() ou à send(), le programme attend qu'un moins un octet de 
données soit lu (ou écrit). Le programme était donc parfois bloqué en attente de données, dans un appel de recv() ou send(),
tandis que le serveur ou le client émettait dans l'autre socket. Nous avons pensé à utilisé des états bloquant ou à utiliser la fonction select(),
mais c'est l'utilisation de fork() qui nous semblait le plus sûr et naturel. \\ 


Par la suite, nous avons été confrontés à des difficultés lors du chargement de certains sites, notamment celui de \url{telecomnancy.eu} et
\url{telecomnancy.univ-lorraine} que le proxy ne supporte pas. Nous ne savons pas encore d'où peux provenir ce problème. D'autres sites, qui utilisent
notamment des trackers statistiques ne sont pas chargés correctement. Nous pensons que ceci peut être du au fait que les trackers détectent 
qu'un proxy est présent, ou bien qu'ils envoient au client une requête qui dépasse la taille de notre buffer d'échange.


\section{Conclusion}

Notre proxy fonctionne correctement sur de nombreux sites, nous rencontrons cependant un quelques problèmes sur certains, notamment lors de la présence de trackers statistiques.
Nous avons été surpris de constater la facilité de création d'un proxy avec les librairies basiques disponibles.
Ce projet nous a rendu le protocole \textsc{HTTP} plus accessible et compréhensible et il fut enrichissant de devoir faire face à la complexité des contenus du web.


\end{document}          
