﻿
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/telnet.h>
#include "Mystrlib.c"
#define SOMAXCON 100

usage(){
  printf("usage : servecho numero_port_serveur\n");
  exit(0);
}

int main(int argc,char* argv[])
{	
	if (argc<2) {
		usage();
	}
	char fromUser[10];

	pid_t pid;
	struct sockaddr_in6 cli_addr, local_addr;
	struct hostent* host;
	int socketClient_co, socketClient_dia;

	bzero((char*)&local_addr,sizeof(local_addr));
	bzero((char*)&cli_addr, sizeof(cli_addr));
	   
	local_addr.sin6_family=AF_INET6;
	local_addr.sin6_port=htons(atoi(argv[1]));
	local_addr.sin6_addr=in6addr_any;
	   

	socketClient_co=socket(AF_INET6,SOCK_STREAM,IPPROTO_TCP);
	if(socketClient_co<0) {
		printf("Problem in initializing socket");
	}
	int no = 0;     
	setsockopt(socketClient_co, IPPROTO_IPV6, IPV6_V6ONLY, (void *)&no, sizeof(no)); 
	int enable = 1;     
	setsockopt(socketClient_co, SOL_SOCKET, SO_REUSEADDR, (void *)&enable, sizeof(enable)); 

	if(bind(socketClient_co,(struct sockaddr*)&local_addr,sizeof(local_addr))<0) {
		printf("Error on binding");
	}
	  
	listen(socketClient_co,SOMAXCON);
	int clilen=sizeof(cli_addr);

	while(1) {

		socketClient_dia=accept(socketClient_co,(struct sockaddr*)&cli_addr,&clilen);

		if(socketClient_dia<0) {
			printf("Problem in accepting connection");
		} else {
			printf("\nWelcome to Magic Proxy ! You are connected\n\n\n");
		}

	 	if ((pid = fork()) == -1)
        {
            close(socketClient_dia);
            continue;
        }


        else if(pid > 0)
        {
            close(socketClient_dia);
            continue;
        }
        else if(pid == 0)
        { //child
            
        int flag=0, socketServ_dia, n, port=0, i;
		char buffer[1000],t1[300],t2[300],t3[10];
		char* temp=NULL;
		char * port2;
		
		bzero((char*)buffer,sizeof(buffer));


		recv(socketClient_dia,buffer,sizeof(buffer),0);
		sscanf(buffer,"%s %s %s",t1,t2,t3);
		char * hostname;
		hostname=cutright(buffer,"Host: ");
    	hostname=cutleft(hostname,"\r\n");

    	//printf("hostname :%s",hostname);
		if(((strncmp(t1,"GET",3)==0))&& ((strncmp(t3,"HTTP/1.1",8)==0)||(strncmp(t3,"HTTP/1.0",8)==0)) 
		 &&   (strncmp(t2,"http://",7)==0) )
		{	 

			struct addrinfo hints;
        	struct addrinfo *result, *rp;
           int s ;
           char address[INET6_ADDRSTRLEN];
           port2 = "80";
   

           /* getaddrinfo */

           memset(&hints, 0, sizeof(struct addrinfo));
           hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
           hints.ai_socktype = SOCK_STREAM; 
           hints.ai_flags = 0;
           hints.ai_protocol = IPPROTO_TCP; 

           s = getaddrinfo(hostname, port2, &hints, &result);
           if (s != 0) {
               fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
               exit(EXIT_FAILURE);
           }

           /* getaddrinfo() returns list of possible addresses.
			This tests each address until connexion success.
           	If  aconnexion fails, we close socket and try with another address */

           for (rp = result; rp != NULL; rp = rp->ai_next) {
               socketServ_dia = socket(rp->ai_family, rp->ai_socktype,
                            rp->ai_protocol);
               if (socketServ_dia == -1)
                   continue;
                
               if(rp->ai_family == AF_INET) {

               	      if( NULL == inet_ntop( AF_INET, &((struct sockaddr_in *)rp->ai_addr)->sin_addr, address, sizeof(address) )) {
               	        perror("inet_ntop");
               	        return EXIT_FAILURE;
               	      }

               	      printf("Connecting to : %s ...\nAddress IPv4 is : %s\n\n", hostname, address);
               	    }

               		if(rp->ai_family == AF_INET6) {

               	      if( NULL == inet_ntop( AF_INET6, &((struct sockaddr_in *)rp->ai_addr)->sin_addr, address, sizeof(address) )) {
               	        perror("inet_ntop");
               	        return EXIT_FAILURE;
               	      }

               	      printf("Connecting to : %s\nAddress IPv6 is:%s\n", hostname, address);
               	    }
               if (connect(socketServ_dia, rp->ai_addr, rp->ai_addrlen) != -1) {
               	
               	//printf("Connected successfuly to distant server !\n");

                break;                  /* Success */
               }

             close(socketServ_dia);
           }

           if (rp == NULL) {               /* No address succeeded */
               fprintf(stderr, "Could not connect\n");
               exit(EXIT_FAILURE);
           }

           freeaddrinfo(result);   


			printf("Request is :\n\n%s",buffer);
			
			n=send(socketServ_dia,buffer,strlen(buffer),0);
			if(n<0)
			printf("Error writing to socket");
			else{
		
				pid = fork();
				if(pid == 0){
					//son
					char buff[1000];
					while(1){
						bzero((char*)buff,sizeof(buff));
						n=recv(socketClient_dia,buff,sizeof(buff),0); 
						if(n>0){
							send(socketServ_dia,buff,n,0);
						}
						
						if(n<=0){
							goto closing;
						}
					}
				}

				else{
					char buff2[1000];
					while(1){
						//father
						bzero((char*)buff2,sizeof(buff2));
						n=recv(socketServ_dia,buff2,sizeof(buff2),0);
						if(n>0){
							send(socketClient_dia,buff2,n,0);
						}
						
						if(n<=0) {
			
							goto closing ;
						}
					
					}
				
				}
				
			}
			
		}
		else
		{
			send(socketClient_dia,"400 : BAD REQUEST\nONLY HTTP REQUESTS ALLOWED",18,0);
		}
         closing :
        //printf("sockets closed\n");
		close(socketClient_dia);
		close(socketServ_dia);
        exit(0);
        }
    }

  close(socketClient_co) ;
  return 0;

}
